const EventSource = require('eventsource');

const url = 'https://stream.wikimedia.org/v2/stream/recentchange';
const eventSource = new EventSource(url);

eventSource.onopen = () => {
    console.info('Opened connection.');
};
eventSource.onerror = (event) => {
    console.error('Encountered error', event);
};
eventSource.onmessage = (event) => {
    const data = JSON.parse(event.data);
    if (data.server_name === 'en.wikipedia.org') {
        // Output the page title
        console.log(data.title);
    }
};

